#include <iostream>
#include <algorithm>
#include <string.h>
#include <map>
#include <unordered_map>
#include <set>

using namespace std;

enum ItemRarity
{
	Common,
	Rare,
	Epic,
	Legendary
};

class Item
{
private:

	string itemName;

public:

	Item()
	{
		itemName = "None";
	}

	Item(string newName)
	{
		itemName = newName;
	}

	string GetName()
	{
		return itemName;
	}

	friend ostream& operator<<(ostream& out, const Item& it)
	{
		out << it.itemName << endl;
		return out;
	}
};

int main()
{
	map<ItemRarity, Item*> rarityMap;
	rarityMap[Legendary] = new Item("Legendary Map Sword");
	rarityMap[Epic] = new Item("Epic Map Axe");
	rarityMap[Rare] = new Item("Rare Map PickAxe");
	rarityMap[Common] = new Item("Common Map Bow");

	unordered_map<ItemRarity, Item*> unorderedRarityMap;
	unorderedRarityMap[Legendary] = new Item("Legendary Unordered Sword");
	unorderedRarityMap[Epic] = new Item("Epic Unordered Axe");
	unorderedRarityMap[Rare] = new Item("Rare Unordered PickAxe");
	unorderedRarityMap[Common] = new Item("Common Unordered Bow");

	set<Item*> raritySet =
	{
		new Item("Legendary Set Sword"),
		new Item("Epic Set Axe"),
		new Item("Rare Set PickAxe"),
		new Item("Common Set Bow")
	};

	cout << "Legendary item found in rarityMap: " << *rarityMap.find(Legendary)->second;

	//
	cout << endl;

	int legendaryCount = count_if(unorderedRarityMap.begin(), unorderedRarityMap.end(), 
		[](pair<ItemRarity, Item*> p) { return (p.first == Legendary); });

	cout << "unorderedRarityMap has " << legendaryCount << " legendary item(s)" << endl;

	//
	cout << endl;

	cout << "raritySet contains:" << endl;

	for_each(raritySet.begin(), raritySet.end(), [](Item* p) { cout << *p; });
}



